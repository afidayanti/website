<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GaleriController;
use App\Http\Controllers\FotoController;

//landing page
Route::get('/', function () {
    return view('umum.index');
}); 

//login
Route::get('/register', 'AuthController@isidata');
Route::post('/kirim', 'AuthController@send');
Route::get('/home', 'HomeController@index')->name('home');

//CRUD  
Route::resource('/user', 'UserController'); 
Route::resource('/kategori', 'KategoriController'); 
Route::resource('/profile', 'ProfileController'); 
Route::resource('/galeri', 'GaleriController'); 
Route::resource('/foto', 'FotoController'); 
Auth::routes();




//Route::get('/profile/create', 'ProfileController@create');
//Route::post('/profile/create', 'ProfileController@create');
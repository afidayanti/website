<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\galeri;
use App\users;
use File;

class GaleriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
      }

    public function index()
    {
    $galeri = Galeri::all();
       return view('galeri.index', compact('galeri'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = users::all();
        return view('galeri.create', compact('users')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',  
            'gambar' => 'required|mimes:png,jpg,jpeg',
            'users_id' => 'required',
        ]);
 


    $fileName = time().'.'.$request->gambar->extension(); 
    $users = new Galeri; 
    $users->judul = $request->judul; 
    $users->gambar = $fileName;
    $users->users_id = $request->users_id; 
    $users->save(); 
    $request->gambar->move(public_path('gm'), $fileName); 
    return redirect('galeri/create'); 
} 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $galeri = Galeri::findorfail($id);
        return view('galeri.show', compact('galeri'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = users::all(); 
        $galeri = Galeri::findorfail($id);
        return view('galeri.edit', compact('galeri','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',  
            'gambar' => 'mimes:png,jpg,jpeg',
            'users_id' => 'required',
        ]);

        $galeri = Galeri::findorfail($id);
        if ($request->has('gambar')) {

            $path = "gm/";
            File::delete($path . $galeri->gambar);
            $fileName = time().'.'.$request->gambar->extension(); 

            $request->gambar->move(public_path('gm'), $fileName); 

            $galeri_data = [
                'judul' => $request->judul,
                'gambar' => $fileName,
                'users_id' => $request->users_id,
            ];
        }else {
            $galeri_data = [
                'judul' => $request->judul, 
                'users_id' => $request->users_id,
            ]; 
        }
        $galeri->update($galeri_data);
        return redirect('/galeri');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $galeri = Galeri::findorfail($id);

        $path = "gm/";
        File::delete($path . $galeri->gambar);
        $galeri->delete();

        return redirect('/galeri');
    }
}

@extends('admin.master')

@section('content')
  <div class="row">
    <div class="col-lg-12"> 
      <div class="card">
        <div class="card-body"> 
          <h5 class="card-title">Tambah Data Galeri</h5>
  
          <!-- General Form Elements --> 
          <form method="POST" action="/galeri" enctype="multipart/form-data">
            @csrf 
            <div class="row mb-3">
              <label  class="col-sm-2 col-form-label">Judul</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="judul" placeholder="Masukkan judul">
                @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                 @enderror
              </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Upload Gambar</label>
                <div class="col-sm-10">
                  <input class="form-control" type="file" name="gambar">
                </div>
              </div> 
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label">User ID</label>
                <div class="col-sm-10">
                  <select class="form-select" name="users_id" id="#">
                      <option>---Pilih ID User Anda---</option>
                    @foreach ($users as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                        
                    @endforeach
                  </select>
                </div>
              </div>  
              <!--<div class="row mb-3">
                <label class="col-sm-2 col-form-label">User ID</label>
                <div class="col-sm-10">
                  <select class="form-select" name="users_id" id="#">
                      <option>---Pilih ID User Anda---</option>
                    @foreach ($users as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                        
                    @endforeach
                  </select>
                </div>
              </div> -->
              <div class="row mb-3"> 
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Create</button>   
                 <a href="{{ route('galeri.index')}}" class="btn btn-danger">Cancel</a>
                </div>
              </div>  
          </form>  
        </div>
      </div> 
    </div> 
  </div>
@endsection
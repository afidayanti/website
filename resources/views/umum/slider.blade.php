
<div class="owl-testimonial owl-carousel owl-theme mb-md-0 mb-sm-5 mb-4">
                <div class="item">
                    <div class="row slider-info">
                        <div class="col-lg-8 message-info align-self">
                            <span class="label-blue mb-sm-4 mb-3">Koleksi</span>
                            <h3 class="title-big mb-4">Mengoleksi hasil foto selfie Anda sendiri? Mengapa tidak?
                            </h3>
                            <p class="message">Album foto tidak pernah lekang oleh waktu untuk menyimpan potret setiap momen hidup. Merek Susan Photo Album, Kokuyo, dan sebagainya memberikan pilihan untuk memastikan foto bersejarah Anda tersimpan dengan baik.</p>
                        </div>
                        <div class="col-lg-4 col-md-8 img-circle mt-lg-0 mt-4">
                            <img src="{{ asset('/landing/images/beauty.jpg')}}" class="img-fluid radius-image-full" alt="client image">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row slider-info">
                        <div class="col-lg-8 message-info align-self">
                            <span class="label-blue mb-sm-4 mb-3">Kenangan</span>
                            <h3 class="title-big mb-4">Foto Anak  adalah Kenangan Masa Kecil mereka ketika mereka sudah dewasa
                            </h3>
                            <p class="message">Apakah kamu memiliki kenangan masa kecil yang masih kental dalam ingatanmu? Kalo iya, itu artinya kamu punya daya ingat yang sangat kuat. Kalo saya ingat tetapi samar-samar. Kita bahas hal-hal yang ada di masa kecil saya ya, apakah hal-hal ini ada juga di masa kecil kamu? Yuk kita cek!</p>
                        </div>
                        <div class="col-lg-4 col-md-8 img-circle mt-lg-0 mt-4">
                            <img src="{{ asset('/landing/images/fashion.jpg')}}" class="img-fluid radius-image-full" alt="client image">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row slider-info">
                        <div class="col-lg-8 message-info align-self">
                            <span class="label-blue mb-sm-4 mb-3">inspirasi foto  makanan</span>
                            <h3 class="title-big mb-4">Makanan yang kita sukai sekarang lagi tren untuk difoto
                            </h3>
                            <p class="message">Mulai dari street food, food truck, hingga toko kue artis. Kuliner kini jadi daya tarik tersendiri saat traveling</p>
                        </div>
                        <div class="col-lg-4 col-md-8 img-circle mt-lg-0 mt-4">
                            <img src="{{ asset('/landing/images/food.jpg')}}" class="img-fluid radius-image-full" alt="client image">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row slider-info">
                        <div class="col-lg-8 message-info align-self">
                            <span class="label-blue mb-sm-4 mb-3">Lifestyle</span>
                            <h3 class="title-big mb-4">To succeed in Life, you need 3 things : a wishbone, a backbone, a
                                funny bone.
                            </h3>
                            <p class="message">Fotografi fashion adalah aliran fotografi yang berkonsentrasi pada memotret dan menampilkan berbagai mode pakaian dan barang-barang fashion lainnya, yang terkait dengan gaya-hidup/life-style yang sedang berjalan pada masa tersebut, untuk diterbitkan di majalah fashion, industri periklanan, atau beredar di kalangan .</p>
                        </div>
                        <div class="col-lg-4 col-md-8 img-circle mt-lg-0 mt-4">
                            <img src="{{ asset('/landing/images/lifestyle.jpg')}}" class="img-fluid radius-image-full"
                                alt="client image">
                        </div>
                    </div>
                </div>
            </div>
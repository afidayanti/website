@extends('admin.master')

@section('content')
<h5 class="card-title">Detail Galeri</h5>
<div class="card mb-3">
    <div class="row g-0"> 
        <img src="{{asset('gambar/'.$galeri->gambar)}}" width="500px"  class="img-fluid rounded-start" alt="...">
       
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">{{$galeri->judul}}</h5>
        </div>
      </div>
      <div class="row mb-3"> 
        <div class="col-sm-10">
          <a href="{{ route('galeri.index')}}" class="btn btn-success">Back</a> 
        </div>
      </div> 
    </div>
  </div>
  @endsection
<div class="d-flex align-items-center justify-content-between">
      <a href="index.html" class="logo d-flex align-items-center">
        <img src="{{ asset('/pemilik/assets/img/logo.png')}}" alt="">
        <span class="d-none d-lg-block">NiceAdmin</span>
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo --> 
    <nav class="header-nav ms-auto">
      <ul class="d-flex align-items-center">  
        <li class="nav-item dropdown pe-3"> 
          <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
            <img src="{{ asset('/pemilik/img/img.png')}}" alt="Profile" class="rounded-circle">
            <span> {{Auth::user()->name}}</span>
          </a><!-- End Profile Iamge Icon --> 
        </li><!-- End Profile Nav -->  
        
      </ul>
    </nav><!-- End Icons Navigation -->
<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;  
use Illuminate\Support\Facades\DB;
use App\kategori;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $this->middleware('auth');
      }

    public function index()
    {
        $kategori = kategori::all(); 

        return view('kategori.index', compact('kategori'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required', 
        ],
        [
            'nama.required' => 'Nama Kategori harus diisi', 
        ]);  

        $kategori = kategori::create([
            'nama' => $request['nama'], 
        ]);
        return redirect('/kategori'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
        $kategori = kategori::find($id);
        
        return view('kategori.show', compact('kategori'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
       $kategori = kategori::find($id);
       return view('kategori.edit', compact('kategori'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
       'nama' => 'required', 
        ],
        [
            'nama.required' => 'nama kategori harus diisi',
        ]); 

        $update = kategori::where('id', $id)->update([
            'nama' => $request['nama'], 
        ]);
            return redirect('/kategori');
           
        } 

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\kategori  $kategori
     * @return \Illuminate\Http\Response
     */
        public function destroy($id)
        {
          
          kategori::destroy($id); 
          return redirect('/kategori'); 
       } 
}

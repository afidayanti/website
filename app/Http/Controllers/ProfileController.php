<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\profile;
//use App\user;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        
        $profile = profile::where('users_id', Auth::id())->first(); 
        return view('profile.index', compact('profile'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
       //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
      //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
       'nama' => 'required', 
       'username' => 'required',  
       'bio' => 'required',  
        ],
        [
            'nama.required' => 'nama profile harus diisi',
            'username.required' => 'username profile harus diisi', 
            'bio.required' => 'bio profile harus diisi', 
        ]); 

        profile::where('id', $id)->update([
            'nama' => $request['nama'], 
            'username' => $request['username'], 
            'bio' => $request['bio'], 
        ]);
            return redirect('/profile');
           
        } 

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\profile  $profile
     * @return \Illuminate\Http\Response
     */
        public function destroy($id)
        {
          
        //
       } 
}

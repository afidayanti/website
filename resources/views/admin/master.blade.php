<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Page Admin</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('/pemilik/img/favicon.png')}}" rel="icon">
  <link href="{{ asset('/pemilik/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('/pemilik/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ asset('/pemilik/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{ asset('/pemilik/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{ asset('/pemilik/vendor/quill/quill.snow.css')}}" rel="stylesheet">
  <link href="{{ asset('/pemilik/vendor/quill/quill.bubble.css')}}" rel="stylesheet">
  <link href="{{ asset('/pemilik/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{ asset('/pemilik/vendor/simple-datatables/style.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('/pemilik/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.2.1
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">

    <!-- HEADER DISNII-->
      @include('admin.header')

  </header><!-- End Header -->

  <!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar"> 
    <ul class="sidebar-nav" id="sidebar-nav"> 
       <!-- sidebar DISNII-->
       @include('admin.aside')
    </ul>

  </aside><!-- End Sidebar-->

  <main id="main" class="main"> 

    <section class="section">
       <!-- sidebar DISNII-->
       @yield('content')
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright <strong><span>NiceAdmin</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
      Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('/pemilik/vendor/apexcharts/apexcharts.min.js')}}"></script>
  <script src="{{ asset('/pemilik/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('/pemilik/vendor/chart.js/chart.min.js')}}"></script>
  <script src="{{ asset('/pemilik/vendor/echarts/echarts.min.js')}}"></script>
  <script src="{{ asset('/pemilik/vendor/quill/quill.min.js')}}"></script>
  <script src="{{ asset('/pemilik/vendor/simple-datatables/simple-datatables.js')}}"></script>
  <script src="{{ asset('/pemilik/vendor/tinymce/tinymce.min.js')}}"></script>
  <script src="{{ asset('/pemilik/vendor/php-email-form/validate.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('/pemilik/js/main.js')}}"></script>

</body>

</html>
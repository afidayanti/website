@extends('admin.master')

@section('content')
<div class="row">
    <div class="col-lg-12"> 
      <div class="card">
        <div class="card-body"> 
          <h5 class="card-title">Edit Data user</h5>
  
          <!-- General Form Elements --> 
          <form method="post" action="/profile/{{$profile->id}}" enctype="multipart/form-data">
            @csrf
            @method('put') 
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Nama </label>
              <div class="col-sm-10">
                <input type="text" name="nama" class="form-control" value="{{ $profile->users->name }}">
              </div>
            </div> 
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Username </label>
              <div class="col-sm-10">
                <input type="text" name="username" class="form-control" value="{{ $profile->username }}">
              </div>
            </div>   
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Bio </label>
              <div class="col-sm-10">
                <input type="text" name="bio" class="form-control" value="{{ $profile->bio }}">
              </div>
            </div>
            <div class="row mb-3"> 
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Update</button> 
              </div>

            </div> 
          </form><!-- End General Form Elements -->
  
        </div>
      </div> 
    </div> 
  </div>
@endsection
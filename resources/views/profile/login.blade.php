@extends('landing.master')

@section('title')
    Create Profile
@endsection

@section('content')
<form action="/profile" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Nama</label>
        <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="title">username</label>
        <input type="text" class="form-control" name="username" id="username" placeholder="Masukkan username Anda">
        @error('username')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="body">Profile Picture</label>
        <input type="file" name="file">
        @error('file')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Save</button>
</form>
@endsection
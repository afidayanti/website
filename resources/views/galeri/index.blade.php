@extends('admin.master')

@section('content')
<div class="col-lg-12">  
      <h5 class="card-title">Data Galeri</h5>
        <a href="{{ route('galeri.create') }}" class="btn btn-primary rounded-pill">Tambah Data</a> <br><br>
          <div class="row"> 
              @forelse ($galeri as $item) 
                <div class="col-lg-6">
                  <!-- Card with an image on top -->
                  <div class="card">
                    <img src="{{asset('gm/'.$item->gambar)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$item->judul}}</h5>
                       
                        <form action="{{ route('galeri.destroy', $item->id)}}" method="POST">
                          @csrf
                          <a href="{{route('galeri.show', $item->id)}}" class="btn btn-info rounded-pill">Detail</a>
                          <a href="{{route('galeri.edit', $item->id)}}" class="btn btn-warning rounded-pill">Edit</a> 
                          @method('delete')
                          <input type="submit" class="btn btn-danger rounded-pill" value="Delete">
                        </form>
                      
                      </div>
                  </div><!-- End Card with an image on top -->
                </div>
                @empty
                <p> Kosong</p>
              @endforelse 
  </div>
</div>
@endsection
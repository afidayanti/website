@extends('admin.master')

@section('content')
<div class="row">
    <div class="col-lg-4 col-md-6 item">
        <div class="card">
            <div class="card-header p-0 position-relative">
                <a href="#blog-single.html">
                    <img class="card-img-bottom d-block radius-image-full" src="{{ asset('/landing/images/fashion1.jpg')}}"
                        alt="Card image cap">
                </a>
            </div>
            <div class="card-body blog-details">
                <p>Fashion styles</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-6 item">
            <div class="card">
                <div class="card-header p-0 position-relative">
                    <a href="#blog-single.html">
                        <img class="card-img-bottom d-block radius-image-full" src="{{ asset('/landing/images/a1.jpg')}}"
                            alt="Card image cap">
                    </a>
                </div>
                <div class="card-body blog-details">
                    <p>Selfi today</p>
                    </div>
                </div>
            </div>
        </div>
@endsection
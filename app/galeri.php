<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class galeri extends Model
{
    protected $table ='galeri';
    protected $fillable = [ 
        'judul', 'gambar', 'users_id'
    ];
   // public function kategori()
   // {
   //   return $this->belongsTo('App\kategori','foto,', 'galeri_id', 'kategori_id');    
   // }
   public $timestamps = false;

   

}

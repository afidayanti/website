<li class="nav-item">
    <a class="nav-link" href="{{ asset('home')}}">
      <i class="bi bi-grid"></i>
      <span>Dashboard</span>
    </a>
  </li><!-- End Dashboard Nav --> 
  <li class="nav-item">
    <a class="nav-link collapsed" href="{{ asset('user')}}">
      <i class="bi bi-journal-text"></i><span>Data Akun</span> 
    </a> 
  </li><!-- End user  --> 
  <li class="nav-item">
    <a class="nav-link collapsed" href="{{ asset('profile')}}">
      <i class="bi bi-person"></i>
      <span>Edit Profile </span>
    </a>
  </li><!-- End Profile Page Nav --> 
  <li class="nav-item">
    <a class="nav-link collapsed" href="{{ asset('kategori')}}">
      <i class="bi bi-bar-chart"></i><span>Kategori</span> 
    </a> 
  </li><!-- End Charts Nav -->  
  <li class="nav-item">
    <a class="nav-link collapsed" href="{{ asset('galeri')}}">
      <i class="bi bi-image"></i><span>Galeri</span> 
    </a>
  </li><!-- End Tables Nav --> 
    <li class="nav-item">
    <a class="nav-link collapsed" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
    </form>
  </li>   
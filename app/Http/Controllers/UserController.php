<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use App\user;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
      }

    public function index()
    {
        $user = user::all(); 

        return view('user.index', compact('user'));  
    }

  /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'password' => 'required', 
        ],
        [
            'nama.required' => 'Nama user harus diisi', 
            'email.required' => 'email user harus diisi', 
            'password.required' => 'password user harus diisi', 
        ]);  

        $user = user::create([
            'nama' => $request['nama'], 
            'email' => $request['email'], 
            'password' => $request['password'], 
        ]);
        return redirect('/user'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
        $user = user::find($id);
        
        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
       $user = user::find($id);
       return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'password' => 'required',
        ],
        [
            'nama.required' => 'Nama user harus diisi', 
            'email.required' => 'email user harus diisi', 
            'password.required' => 'password user harus diisi',
        ]); 

        $update = user::where('id', $id)->update([
            'nama' => $request['nama'], 
            'email' => $request['email'], 
            'password' => $request['password'], 
        ]);
            return redirect('/user');
           
        } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
        {
          
            user::destroy($id); 
          return redirect('/user'); 
       } 
}
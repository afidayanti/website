<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $table ='foto';
    protected $quarded = [];

    public function galeri()
    {
     return $this->belongsTo('App\galeri');
    }  
    public function kategori()
    {
     return $this->belongsToMany('App\kategori');
    }
}

@extends('umum.master')

@section('content') 
    <!--kategori1-->
<div class="left-right">
    <h3 class="section-title-left mb-sm-4 mb-2"> KATEGORI: Lifestyle</h3> 
</div>
<div class="row">
    <div class="col-lg-4 col-md-6 item">
        <div class="card">
            <div class="card-header p-0 position-relative">
                <a href="#blog-single.html">
                    <img class="card-img-bottom d-block radius-image-full" src="{{ asset('/landing/images/fashion1.jpg')}}"
                        alt="Card image cap">
                </a>
            </div>
            <div class="card-body blog-details">
                <a href="#blog-single.html" class="blog-desc">An easy Guide to buying Denim & My favourite styles
                </a>
                <div class="author align-items-center">
                    <img src="{{ asset('/landing/images/a1.jpg')}}" alt="" class="img-fluid rounded-circle" />
                    <ul class="blog-meta">
                        <li>
                            <a href="author.html">Isabella ava</a> </a>
                        </li>
                        <li class="meta-item blog-lesson">
                            <span class="meta-value"> July 13, 2020 </span>. <span
                                class="meta-value ml-2"><span class="fa fa-clock-o"></span> 1 min</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 mt-md-0 mt-4">
        <div class="card">
            <div class="card-header p-0 position-relative">
                <a href="#blog-single.html">
                    <img class="card-img-bottom d-block radius-image-full" src="{{ asset('/landing/images/fashion2.jpg')}}"
                        alt="Card image cap">
                </a>
            </div>
            <div class="card-body blog-details">
                <a href="#blog-single.html" class="blog-desc">The Satin Skirt & How to Wear it all year round fashion
                </a>
                <div class="author align-items-center">
                    <img src="{{ asset('/landing/images/a2.jpg')}}" alt="" class="img-fluid rounded-circle" />
                    <ul class="blog-meta">
                        <li>
                            <a href="author.html">Charlotte mia</a> </a>
                        </li>
                        <li class="meta-item blog-lesson">
                            <span class="meta-value"> July 13, 2020 </span>. <span
                                class="meta-value ml-2"><span class="fa fa-clock-o"></span> 1 min</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 mt-md-0 mt-4">
        <div class="card">
            <div class="card-header p-0 position-relative">
                <a href="#blog-single.html">
                    <img class="card-img-bottom d-block radius-image-full" src="{{ asset('/landing/images/fashion3.jpg')}}"
                        alt="Card image cap">
                </a>
            </div>
            <div class="card-body blog-details">
                <a href="#blog-single.html" class="blog-desc">What I’ll be Wearing this Party Season & the Festive edit
                </a>
                <div class="author align-items-center">
                    <img src="{{ asset('/landing/images/a3.jpg')}}" alt="" class="img-fluid rounded-circle" />
                    <ul class="blog-meta">
                        <li>
                            <a href="author.html">Elizabeth</a> </a>
                        </li>
                        <li class="meta-item blog-lesson">
                            <span class="meta-value"> July 13, 2020 </span>. <span
                                class="meta-value ml-2"><span class="fa fa-clock-o"></span> 1 min</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--KATEGORI 2-->
<br><br>
<div class="left-right">
    <h3 class="section-title-left mb-sm-4 mb-2"> KATEGORI: Album terbaru</h3> 
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="bg-clr-white hover-box">
            <div class="row">
                <div class="col-sm-5 position-relative">
                    <a href="#blog-single.html" class="image-mobile">
                        <img class="card-img-bottom d-block radius-image-full" src="{{ asset('/landing/images/beauty1.jpg')}}"
                            alt="Card image cap">
                    </a>
                </div>
                <div class="col-sm-7 card-body blog-details align-self">
                    <a href="#blog-single.html" class="blog-desc">Natural Spa - Where you feel unique and special
                    </a>
                    <div class="author align-items-center">
                        <img src="{{ asset('/landing/images/a1.jpg')}}" alt="" class="img-fluid rounded-circle" />
                        <ul class="blog-meta">
                            <li>
                                <a href="author.html">Isabella ava</a> </a>
                            </li>
                            <li class="meta-item blog-lesson">
                                <span class="meta-value"> July 13, 2020 </span>. <span
                                    class="meta-value ml-2"><span class="fa fa-clock-o"></span> 1 min</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 mt-lg-0 mt-4">
        <div class="bg-clr-white hover-box">
            <div class="row">
                <div class="col-sm-5 position-relative">
                    <a href="#blog-single.html" class="image-mobile">
                        <img class="card-img-bottom d-block radius-image-full" src="{{ asset('/landing/images/beauty2.jpg')}}"
                            alt="Card image cap">
                    </a>
                </div>
                <div class="col-sm-7 card-body blog-details align-self">
                    <a href="#blog-single.html" class="blog-desc">How to get Beautiful coloring Highlights this weak
                    </a>
                    <div class="author align-items-center">
                        <img src="{{ asset('/landing/images/a2.jpg')}}" alt="" class="img-fluid rounded-circle" />
                        <ul class="blog-meta">
                            <li>
                                <a href="author.html">Charlotte mia</a> </a>
                            </li>
                            <li class="meta-item blog-lesson">
                                <span class="meta-value"> July 13, 2020 </span>. <span
                                    class="meta-value ml-2"><span class="fa fa-clock-o"></span> 1 min</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 mt-4">
        <div class="bg-clr-white hover-box">
            <div class="row">
                <div class="col-sm-5 position-relative">
                    <a href="#blog-single.html" class="image-mobile">
                        <img class="card-img-bottom d-block radius-image-full" src="{{ asset('/landing/images/beauty3.jpg')}}"
                            alt="Card image cap">
                    </a>
                </div>
                <div class="col-sm-7 card-body blog-details align-self">
                    <a href="#blog-single.html" class="blog-desc">New Hair Styles can look fabulous and shiny
                    </a>
                    <div class="author align-items-center">
                        <img src="{{ asset('/landing/images/a3.jpg')}}" alt="" class="img-fluid rounded-circle" />
                        <ul class="blog-meta">
                            <li>
                                <a href="author.html">Elizabeth</a> </a>
                            </li>
                            <li class="meta-item blog-lesson">
                                <span class="meta-value"> July 13, 2020 </span>. <span
                                    class="meta-value ml-2"><span class="fa fa-clock-o"></span> 1 min</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 mt-4">
        <div class="bg-clr-white hover-box">
            <div class="row">
                <div class="col-sm-5 position-relative">
                    <a href="#blog-single.html" class="image-mobile">
                        <img class="card-img-bottom d-block radius-image-full" src="{{ asset('/landing/images/beauty4.jpg')}}"
                            alt="Card image cap">
                    </a>
                </div>
                <div class="col-sm-7 card-body blog-details align-self">
                    <a href="#blog-single.html" class="blog-desc">How our dreams should shine and spray beauty to us.</a>
                    <div class="author align-items-center">
                        <img src="{{ asset('/landing/images/a1.jpg')}}" alt="" class="img-fluid rounded-circle" />
                        <ul class="blog-meta">
                            <li>
                                <a href="author.html">Isabella ava</a> </a>
                            </li>
                            <li class="meta-item blog-lesson">
                                <span class="meta-value"> July 13, 2020 </span>. <span
                                    class="meta-value ml-2"><span class="fa fa-clock-o"></span> 1 min</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
 
@extends('admin.master')

@section('content')
  <div class="row">
    <div class="col-lg-12"> 
      <div class="card">
        <div class="card-body"> 
          <h5 class="card-title">Tambah Data Kategori</h5>
  
          <!-- General Form Elements --> 
          <form method="POST" action="/kategori" enctype="multipart/form-data">
            @csrf 
            <div class="row mb-3">
              <label  class="col-sm-2 col-form-label">Nama Kategori</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Kategori">
                @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                 @enderror
              </div>
            </div>
              <div class="row mb-3"> 
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Create</button>   
                  <a href="{{ route('kategori.index')}}" class="btn btn-danger">Cancel</a>
                </div>
              </div>  
          </form>  
        </div>
      </div> 
    </div> 
  </div>
@endsection
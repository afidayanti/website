@extends('admin.master')

@section('content')
<div class="col-lg-12"> 
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Data User</h5>
   
       <!--  <a href="{{ route('register') }}" class="btn btn-primary rounded-pill">Tambah Data</a>
        Table with stripped rows -->
  
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><p>{{ $message }}</p></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif
         <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Name</th> 
              <th scope="col">Email</th> 
              <th scope="col">Action</th>
            </tr> 
          </thead>
          @foreach ($user as $key => $user)
          <tbody>
            <tr>
              <th>{{ $key + 1 }}</th>
              <td>{{ $user->name }}</td> 
              <td>{{ $user->email }}</td> 
              <form action="/user/{{$user->id}}" method="POST">
                <td>  
                  <a href="{{ route('user.show', ['user' => $user->id])}}" class="btn btn-info rounded-pill">Detail</a>
                  <!--<a href="{{ route('user.edit', ['user' => $user->id])}}" class="btn btn-warning rounded-pill">Edit</a>
                  
                  @csrf
                  @method('delete')
                  <button type="submit" class="btn btn-danger rounded-pill">Delete</button> -->
                </td>
              </form> 
            </tr> 
          </tbody>
          @endforeach
        </table> 
      </div>
    </div>
  </div> 
@endsection
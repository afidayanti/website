@extends('admin.master')

@section('content')
<div class="row">
    <div class="col-lg-12"> 
      <div class="card">
        <div class="card-body"> 
          <h5 class="card-title">Edit Data Kategori</h5>
  
          <!-- General Form Elements --> 
          <form method="post" action="/kategori/{{$kategori->id}}" enctype="multipart/form-data">
            @csrf
            @method('put') 
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Nama Kategori</label>
              <div class="col-sm-10">
                <input type="text" name="nama" class="form-control" value="{{ $kategori->nama }}">
              </div>
            </div> 
            <div class="row mb-3"> 
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Update</button> 
                <a href="{{ route('kategori.index')}}" class="btn btn-success">Back</a> 
              </div>

            </div> 
          </form><!-- End General Form Elements -->
  
        </div>
      </div> 
    </div> 
  </div>
@endsection
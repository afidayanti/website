<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table ='profile';
    protected $fillable = [ 
        'nama',
        'username',
        'bio',
        'users_id',	
    ];

    public function users()
    {
        return $this->belongsTo('App\users','users_id');    
    }
    

}

@extends('admin.master')

@section('content') 

<div class="row">
    <div class="col-lg-12"> 
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Detail Kategori</h5>
  
          <!-- General Form Elements -->
          <form>  
            <div class="row mb-3">
                <div class="col-lg-3 col-md-4 label ">Nama Kategori : </div>
                <div class="col-lg-9 col-md-8">{{$kategori->nama}}</div>
              </div>

            <div class="row mb-3"> 
              <div class="col-sm-10">
                <a href="{{ route('kategori.index')}}" class="btn btn-success">Back</a> 
              </div>
            </div>
  
          </form><!-- End General Form Elements -->
  
        </div>
      </div>
  
    </div> 
  </div>
  @endsection 